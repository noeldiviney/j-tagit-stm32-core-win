/*
  Copyright (c) 2011 Arduino.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#if defined(ARDUINO_F427ZIT6_DinRDuino_VibosSilo8)
#include "pins_arduino.h"

// Pin number
// Match DinRDuino Dinrail D12 pin assignments
// 
const PinName digitalPin[] = {
 //-------------------------------J4---------------------------------------------------------
  PE_1,  //D0
  PE_0,  //D1
  PB_9,  //D2
  PB_8,  //D3
  PB_7,  //D4
  PB_6,  //D5
  PB_5,  //D6
  PB_4,  //D7
  PG_12, //D8
  PG_9,  //D9
  PD_7,  //D10
  PD_6,  //D11
//-------------------------------J5---------------------------------------------------------
  PD_5,  //D12
  PD_4,  //D13
  PD_3,  //D14
  PD_2,  //D15
  PB_1,  //D16
  PB_0,  //D17
  PA_6,  //D18
  PA_3,  //D19
//-------------------------------J6---------------------------------------------------------
   PD_8, //D20
  PG_7,  //D21
  PG_6,  //D22
  PG_5,  //D23
  PG_4,  //D24
  PG_3,  //D25
  PD_15, //D26
  PD_14, //D27
  PD_13, //D28
  PC_11, //D29
 //-------------------------------J7---------------------------------------------------------
  PF_0,  //D30
  PF_1,  //D31
  PA_4,  //D32
  PA_5,  //D33 
  PD_0,  //D34
  PD_1,  //D35
 //-------------------------------J8---------------------------------------------------------
  PC_10, //D36
  PA_15, //D37
  PA_8,  //D38
  PC_9,  //D39
  PC_8,  //D40
  PD_12, //D41
  PD_8,  //D42
//-------------------------------J9---------------------------------------------------------
  PF_10, //D43
  PC_0,  //D44
  PF_8,  //D45
  PF_9,  //D46
  PC_2,  //D47
  PF_6,  //D48
  PF_7,  //D49
  PF_4,  //D50
  PF_5,  //D51
  PC_3,  //D52
  PF_2,  //D53
  PF_3,  //D54
  PE_6,  //D55
  PC_13, //D56
  PA_0,  //D57
  PE_4,  //D58
  PE_5,  //D59
  PB_2,  //D60
  PE_2,  //D61
  PE_3,  //D62
//-------------------------------J10--------------------------------------------------------
  PE_14, //D63
  PE_15, //D64
  PE_12, //D65
  PE_13, //D66
  PG_15, //D67
  PE_10, //D68
  PE_11, //D69
  PE_8,  //D70
  PE_9,  //D71
  PD_11, //D72
  PG_1,  //D73 - LED_BLUE
  PE_7,  //D74 - LED_RED
  PF_15, //D75 - USER_BTN
  PG_0,  //D76 - Serial Rx
  PD_10, //D77 - Serial Tx
  PF_13, //D78/A0
  PF_14, //D79
  PC_12, //D80
  PF_11, //D81
  PF_12, //D82
//------------------------------------------------------------------------------------------
// Misc
  PC_6,  //D83
  PB_13, //D84
  PB_10, //D85
  PB_11, //D86 
  PA_8,	 //087
  PC_9,  //088
  PA_12, //089
#if 0               // not yet assigned
  PC_0,  //D79/A1
  PC_3,  //D80/A2
  PF_3,  //D81/A3
  PF_5,  //D82/A4
  PF_10, //D83/A5
  PB_1,  //D84/A6
  PC_2,  //D85/A7
  PF_4,  //D86/A8
  PF_6,  //D87/A9
  // Duplicated pins in order to be aligned with PinMap_ADC
  PA_7,  //D88/A10 = D11
  PA_6,  //D89/A11 = D12
  PA_5,  //D90/A12 = D13
  PA_4,  //D91/A13 = D24
  PA_0,  //D92/A14 = D32
  PF_8,  //D93/A15 = D61
  PF_7,  //D94/A16 = D62
  PF_9   //D95/A17 = D63
#endif  // not yet assigned
};

#ifdef __cplusplus
}
#endif

#endif /* ARDUINO_F427ZIT6_DinRDuino_VibosSilo8 */
